import { Gauge } from './fuelGauge';

export function defaultInit(idname) {
  const start = 20;
    const range = (90-start)*2;
    const end=start+range;
  const initData = {
    size: 250,
    minorTicks: 1,
    showPointer: true,
    showValue: true,
    valueFontSize: 12,
    min: start,
    max: start + range,
    up: 0.85,
    low: 0.45,
    margin_radius: 0.04,
    unit: 's',
      redZone: {
          from: start+2*range/3,
          to: end  ,
      },
      yellowZone: {
          from: start+range/3,
          to: start+2*range/3
      },
      greenZone: {
          from: start,
          to:  start+range/3,
      },

  };
    const fgID = idname ? idname:'fuelGauge';
  const fgChart = new Gauge(`#${fgID}`, initData);
  fgChart.render(50);
  return fgChart;
}

export function setButton(chart, buttonid) {
  function updateFuelGauge() {
    const data = Math.random() * 120;
    chart.redraw(data);
  }
  const bid = buttonid ? buttonid:'buttonRandomFuelGauge';
  const buttonForm = document.getElementById(bid);
  buttonForm.onclick = updateFuelGauge;
}

export { Gauge };
