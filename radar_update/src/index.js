let d3=require('d3');
import { RadarChart } from './radarChart';

console.log("D3 scale", d3, d3.scale);
var color = d3.scaleOrdinal().range(["#EDC951","#CC333F","#00A0B0"]);

console.log("Color",color);

var radarChartOptions = {
  width: 500,
  height: 500,
  color: color
};

radarChart = RadarChart();

d3.select('#radarChart')
  .call(radarChart);

radarChart.options(radarChartOptions).update();


!function() {

   var operation = d3.select('body').append('div').append('h2');

   let data = 
      [  
        {  
          "key":"Nokia Smartphone",
          "values":[  
            {  "axis":"Battery Life", "value":0.26 }, {  "axis":"Brand", "value":0.10 },
            {  "axis":"Contract Cost", "value":0.30 }, {  "axis":"Design And Quality", "value":0.14 },
            {  "axis":"Have Internet Connectivity", "value":0.22 }, {  "axis":"Large Screen", "value":0.04 },
            {  "axis":"Price Of Device", "value":0.41 }, {  "axis":"To Be A Smartphone", "value":0.30 }
          ]
        },
        {  
          "key":"Samsung",
          "values":[  
            {  "axis":"Battery Life", "value":0.27 }, {  "axis":"Brand", "value":0.16 },
            {  "axis":"Contract Cost", "value":0.35 }, {  "axis":"Design And Quality", "value":0.13 },
            {  "axis":"Have Internet Connectivity", "value":0.20 }, {  "axis":"Large Screen", "value":0.13 },
            {  "axis":"Price Of Device", "value":0.35 }, {  "axis":"To Be A Smartphone", "value":0.38 }
          ]
        },
        {  
          "key":"iPhone",
          "values":[  
            {  "axis":"Battery Life", "value":0.22 }, {  "axis":"Brand", "value":0.28 },
            {  "axis":"Contract Cost", "value":0.29 }, {  "axis":"Design And Quality", "value":0.17 },
            {  "axis":"Have Internet Connectivity", "value":0.22 }, {  "axis":"Large Screen", "value":0.02 },
            {  "axis":"Price Of Device", "value":0.21 }, {  "axis":"To Be A Smartphone", "value":0.50 }
          ]
        }
      ];

   setTimeout(function() { 
      operation.text(' radarChart.data(data).duration(1000).update(); ');
      radarChart.data(data).duration(1000).update();
   }, 200);
   
    setTimeout(function() { 
        operation.html(" radarChart.options({'legend': {display: true}}); <br> radarChart.colors({'iPhone': 'blue', 'Samsung': 'red', 'Nokia Smartphone': 'yellow'}).update(); ");
        radarChart.options({'legend': {display: true}});
        radarChart.colors({'iPhone': 'blue', 'Samsung': 'red', 'Nokia Smartphone': 'yellow'}).update();
    }, 4000);

}();
