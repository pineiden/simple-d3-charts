let d3 = require("d3");
import { CircleChart } from "./circleChart";

// iniciar gráfico d3 circleChart

let circle_id = "circleChart";

function newCircle() {
  // tomar valor del campo radio
  // tomar color del campo color
  let circleChart = new CircleChart("#" + circle_id, {});
  return circleChart;
}
let init_data = { cx: 100, cy: 100, radius: 20 };
let circle = newCircle();
circle.init(init_data);

// update circle wrapper
function updateCircle() {
  let radius = parseFloat(document.getElementById("c_radius").value);
  let color_select = document.getElementById("c_color");
  let cx = parseFloat(document.getElementById("c_x").value);
  let cy = parseFloat(document.getElementById("c_y").value);
  let color = color_select.options[color_select.selectedIndex].value;
  // get from form
  let new_data = [{ radius: radius, fill: color, cx: cx, cy: cy }];
  circle.update(new_data);
}
let button_form = document.getElementById("buttonChange");
button_form.onclick = updateCircle;
