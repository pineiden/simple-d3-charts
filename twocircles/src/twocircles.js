import { CircleChart } from './circleChart';

const d3 = require('d3');

export default function TwoCirclesChart(parentSelector, options) {
  const w = options.width ? options.width : 60;
  const h = options.height ? options.height : 50;
  const a = options.viewport.w ? options.viewport.w : 9;
  // const b = options.viewport.h ? options.viewport.h : 5;
  const wTimes = w / a;
  // const hTimes = h / b;

  // function to relate value with some colors
  const color = d3
    .scaleLinear()
    .domain([1, 2, 5, 10, 15, 20])
    .range(['greenyellow', 'green', 'cyan', 'blue', 'yellow', 'red']);
  // r0 is rmax
  const r0 = 20;
  // const r1 = 14;
  const separation = options.separation;
    // const { margin } = options;
  const Cx = w / 2;
  const Cy = h / 2;
  const Ry = r0*Math.sqrt(2);
    const Rx = 2*r0+separation/2;
  const c = Math.sqrt(Math.abs(Rx ** (2) - Ry ** (2)));
  const code = options.code;

  const opts = {
    circles: {
      pdop: {
        radius: r0,
        fill: color(r0),
        cx: Cx - c/2 - separation/2,
        cy: Cy,
        font_size: '10px',
        class_name: 'pdop',
        legend: 'PDOP',
      },
      tdop: {
        radius: r0,
        fill: color(r0),
        cx: Cx + c/2 + separation/2,
        cy: Cy,
        font_size: '10px',
        class_name: 'tdop',
        legend: 'TDOP',
      },
    },
    ellipse: {
      active: true,
        ry:Ry,
        rx:Rx,
      cx: Cx,
      cy: Cy,
    },
    margin: 5,
    inner_space: 10,
  };

  // ///////////////////////////////////////////////////////
  // ////////// Create the container SVG and g /////////////
  // ///////////////////////////////////////////////////////
  const parent = d3.select(parentSelector);

  // Remove whatever chart with the same id/class was present before
  parent.select('svg').remove();

  const component = d3
    .select(parentSelector)
    .append('svg')
    .attr('class', 'svg-class')
    .attr('width', w)
    .attr('height', h);

  component
    .append('g')
    .attr('class', 'two_circlesViz')
    .attr('width', w)
    .attr('height', h);

  // Put all of the options into a variable called cfg
  component.refresh = function refreshOpts(optionsRefresh) {
    if (typeof optionsRefresh !== 'undefined') {
      Object.keys(optionsRefresh).forEach((e) => {
        if (typeof optionsRefresh[e] !== 'undefined') {
          opts[e] = optionsRefresh[e];
        }
      });
    }
  };

  component.refresh(options);
  let circlesCmp = { pdop: Object, tdop: Object };

  component.init = function initTwoCircles() {
    if (opts.ellipse.active) {
      const gradient = component
        .append('defs')
        .append('linearGradient')
        .attr('id', `ellipse-gradient-${code}`);
      gradient
        .attr('x1', '10%')
        .attr('y1', '0%')
        .attr('x2', '90%')
        .attr('y2', '0%');
      component.begin_gradient = gradient
        .append('stop')
        .attr('id', `init-gradient-${code}`)
        .attr('offset', '0%')
        .attr('stop-color', color(r0));
      component.end_gradient = gradient
        .append('stop')
        .attr('id', `end-gradient-${code}`)
        .attr('offset', '90%')
        .attr('stop-color', color(r0));
      component
        .append('ellipse')
        .attr('ry', opts.ellipse.ry)
        .attr('rx', opts.ellipse.rx)
        .attr('cx', opts.ellipse.cx)
        .attr('cy', opts.ellipse.cy)
        .attr('fill', `url('#ellipse-gradient-${code}')`)
        .attr('class', 'ellipse' - code);
    }
    // crear circulo 1
    const cPdop = new CircleChart('#pdop', {
      component,
      class_name: 'pdop',
    });
    const cTdop = new CircleChart('#tdop', {
      component,
      class_name: 'tdop',
    });
    cPdop.init(opts.circles.pdop);
    cTdop.init(opts.circles.tdop);
      circlesCmp = { pdop: cPdop, tdop: cTdop };
  };

  component.circle = function getcircle(key) {
    return circlesCmp[key];
  };

  component.update2 = function updateTwoCirclesChart(newData) {
    const dataPdop = [
      {
        radius: newData.PDOP,
        fill: color(newData.PDOP),
        cx: opts.circles.pdop.cx,
        cy: opts.circles.pdop.cy,
      },
    ];
    const dataTdop = [
      {
        radius: newData.TDOP,
        fill: color(newData.TDOP),
        cx: opts.circles.tdop.cx,
        cy: opts.circles.tdop.cy,
      },
    ];
    circlesCmp.pdop.update(dataPdop);
    circlesCmp.tdop.update(dataTdop);
    component.begin_gradient
      .transition()
      .duration(1000)
      .attr('stop-color', color(newData.PDOP));
    if (opts.ellipse.active) {
      component.end_gradient
        .transition()
        .duration(1000)
        .attr('stop-color', color(newData.TDOP));
    }
  };

  component.newcolor = function newcolor(value) {
    return color(value);
  };

  return component;
}

module.exports = Object.assign({
    TwoCirclesChart: TwoCirclesChart},
                               module.exports);
