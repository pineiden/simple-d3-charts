let d3 = require("d3");
import { TwoCirclesChart } from "./twocircles";

export function defaultInit() {
  let color = d3.scaleOrdinal().range(["#EDC951", "#CC333F", "#00A0B0"]);
  let svg_vwp = { w: 120, h: 80 };
  let init_data = {
    width: svg_vwp.w,
    height: svg_vwp.h,
    color: color,
    viewport: svg_vwp,
    separation: 12,
      margin: 10,
      code:'default'
  };
  let twocirclesID = "twocirclesChart";
  let twocirclesChart = TwoCirclesChart("#" + twocirclesID, init_data);
  twocirclesChart.init();
  return twocirclesChart;
}

export function setButton(chart) {
  function updateTwoCircles() {
    let pdop = Math.random() * 20;
    let tdop = Math.random() * 20;
    let data = { PDOP: pdop, TDOP: tdop };
    chart.update2(data);
  }
  let button_form = document.getElementById("buttonRandomTwoCircles");
  button_form.onclick = updateTwoCircles;
}

export {TwoCirclesChart};
