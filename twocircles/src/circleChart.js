const d3 = require('d3');

export default function CircleChart(parentSelector, options) {
  const opts = {
    radius: 10,
    fill: 'red',
    cx: 20,
    cy: 20,
    dx: 20,
    dy: 20 + 10 + 20,
    class_name: 'circle',
    legend: 'Círculo',
    show_legend: true,
  };

  // ///////////////////////////////////////////////////////
  // ////////// Create the container SVG and g /////////////
  // ///////////////////////////////////////////////////////

  // Remove whatever chart with the same id/class was present before
    let component=undefined;
    if (options.hasOwnProperty("component")){
        component = options.component;
    };

  let circle = Object;

  if (!component) {
    component = d3
      .select(parentSelector)
      .append('svg')
      .attr('class', 'svg-class')
      .attr('width', 450)
      .attr('height', 250);
      circle = component;
      circle=component;
  } else {
    circle = component.append('g').attr('class', options.class_name);
  }

  circle
    .append('g')
    .attr('class', 'circleViz')
    .attr('width', 40)
    .attr('height', 40);

  // Put all of the options into a variable called cfg
  circle.refresh = function refreshOpts(optionsRefresh) {
    if (typeof optionsRefresh !== 'undefined') {
      Object.keys(optionsRefresh).forEach((e) => {
        if (typeof optionsRefresh[e] !== 'undefined') {
          opts[e] = optionsRefresh[e];
        }
      });
    }
  };

  circle.refresh(options);

  circle.init = function initCircle(data) {
    this.refresh(data);
    this.selectAll('.circleViz')
      .append('circle')
      .attr('cx', () => opts.cx)
      .attr('cy', () => opts.cy)
      .attr('r', () => opts.radius)
      .attr('fill', () => opts.fill)
      .attr('class', opts.class_name);
    // legend
    this.selectAll('.circleViz')
      .append('text')
      .attr('dx', () => opts.cx)
      .attr('dy', () => opts.cy + opts.radius + 20)
      .attr('class', `text-${opts.class_name}`)
      .attr('text-anchor', 'middle')
      .style('font-size', opts.font_size)
      .text(() => (opts.show_legend ? opts.legend : ''));
  };

  circle.update = function updateCircles(newData) {
    this.selectAll(`.${opts.class_name}`)
      .data(newData)
      .transition()
      .duration(1000)
      .attr('cx', (d) => d.cx)
      .attr('cy', (d) => d.cy)
      .attr('r', (d) => d.radius)
      .attr('fill', (d) => d.fill)
      .transition()
      .duration(1000);

    this.selectAll(`.text-${opts.class_name}`)
      .data(newData)
      .transition()
      .duration(1000)
      .attr('dx', (d) => d.cx)
      .attr('dy', (d) => d.cy + d.radius + 20)
      .attr('text-anchor', 'middle')
      .transition()
      .duration(1000);
  };

  circle.getOpts = function getopts() {
    return opts;
  };

  return circle;
}

export { CircleChart };
