import {
  loadLiquidFillGauge,
  liquidFillGaugeDefaultSettings,
} from './liquidFillGaugeLast';

const gaugeTpl = `
        <div id="item_lfg">
            <div class="value"></div>
            <div class="gauge_status">
                <svg id="chart_lfg" width="100", height="100" onclick=""></svg>
            </div>
        </div>
        `;


export function createGauge(chartId, gaugeParams, initialValue) {
  const varid = 'liquidFillGauge';
  const component = document.getElementById(varid);
  component.innerHTML = gaugeTpl;
  const config = liquidFillGaugeDefaultSettings();
  if (gaugeParams) {
    const gp = gaugeParams;
    Object.keys(config).forEach((key) => {
      if ({}.hasOwnProperty.call(gp, key)) {
        config[key] = gp[key];
      }
    });
  }
  const gauge = loadLiquidFillGauge('chart_lfg', initialValue, config);
  gauge.update(initialValue);
  return gauge;
}


export function defaultInit(fuelgaugeID) {
  const initData = {
    circleThickness: 0.15,
    textVertPosition: 0.8,
    waveAnimate: true,
    textSize: 0.8,
    minValue: 0,
    maxValue: 500,
    waveAnimateTime: 1000,
    waveCount: 5,
    circleColor: '#F4D03F',
    waveColor: '#F5B041',
    textTextColor: '#229954',
    waveTextColor: '#229954',
  };
    const fgID = fuelgaugeID ? fuelgaugeID:'liquidFillGauge';
  const fgChart = createGauge(fgID, initData, 50);
  return fgChart;
}

export function setButton(chart, buttonid) {
  function updateLiquidFuelGauge() {
    const data = Math.random() * chart.config.maxValue;
    chart.update(data);
  }
    const bid = buttonid ? buttonid:'buttonRandomLiquidFillGauge';
  const buttonForm = document.getElementById(bid);
  buttonForm.onclick = updateLiquidFuelGauge;
}

export {
  loadLiquidFillGauge,
  liquidFillGaugeDefaultSettings,
};
