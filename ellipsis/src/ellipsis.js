let d3=require('d3');

const TwoCirclesChart = function TwoCirclesChart(parent_selector, margin, options){
    let margin=5;
    let opts = {
        'pdop':{
            'radius':10,
            'fill':'green',
            'cx': 20+margin,
            'cy': 20+margin,
            'class_name':'pdop'},
        'tdop':{
            'radius':10,
            'fill':'green',
            'cx': 20*2+margin+10,
            'cy': 20+margin,
            'class_name':'tdop'},
    };


	/////////////////////////////////////////////////////////
	//////////// Create the container SVG and g /////////////
	/////////////////////////////////////////////////////////
	const parent = d3.select(parent_selector);

};
